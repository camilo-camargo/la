OBJ=$(wildcard */*.c)

all: la main

la: 
		$(foreach LA_OBJ,$(OBJ),cc -c $(LA_OBJ) -o $(patsubst %.c,%.o,$(LA_OBJ));) 
	cc -lX11 -lm $(patsubst %.c, %.o, $(OBJ)) -o main main.c
	./main

clean:
	rm -rf $(OBJ)
