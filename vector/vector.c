#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "../point/point.h"
#include "vector.h" 

static Point_2D origin = {0,0};
static Point_2D unary_i_point = {1,0};
static Point_2D unary_j_point = {0,1}; 
static Vector_2D unary_i = {&origin, &unary_i_point};
static Vector_2D unary_j = {&origin, &unary_j_point};


Vector_2D *vector_2d_ctor(Point_2D *head, Point_2D*tail){
	assert(head != NULL);	
	assert(tail != NULL);
	Vector_2D *vector = (Vector_2D*) malloc(sizeof(Vector_2D));
	vector->head = head;
	vector->tail = tail;
	return vector;
} 

Vector_2D *vector_2d_origin(Vector_2D *v){
	return vector_2d_ctor(&origin, point_2d_ctor(v->tail->x - v->head->x,v->tail->y - v->head->y));
} 


Vector_2D *vector_2d_add(Vector_2D* v, Vector_2D* w){
	Vector_2D *v1 = vector_2d_origin(v);
	Vector_2D *v2 = vector_2d_origin(w);
	v1->tail->x += v2->tail->x;
	v1->tail->y += v2->tail->y;  
	vector_2d_dtor(v2);
	return v1;
}
Vector_2D *vector_2d_sub(Vector_2D* v, Vector_2D* w){
	Vector_2D *v1 = vector_2d_origin(v);
	Vector_2D *v2 = vector_2d_origin(w);
	v1->tail->x -= v2->tail->x;
	v1->tail->y -= v2->tail->y;  
	vector_2d_dtor(v2);
	return v1;
}
Vector_2D *vector_2d_mul(double scalar, Vector_2D *v){
	v->tail->x *= scalar;
	v->tail->y *= scalar;
	return v;
} 

double vector_2d_magnitude(Vector_2D *v){ 
	Vector_2D *vector = vector_2d_origin(v);
	double magnitude = sqrt(vector->tail->x * vector->tail->x + vector->tail->y * vector->tail->y);
	free(vector);
	return magnitude;
} 

double vector_2d_dot(Vector_2D* v, Vector_2D* w){
	Vector_2D *v1 = vector_2d_origin(v);
	Vector_2D *v2 = vector_2d_origin(w);
	double result;
	result += v1->tail->x * v2->tail->x;
	result += v1->tail->y * v2->tail->y;
	vector_2d_dtor(v1);
	vector_2d_dtor(v2);
	return result;
}

double vector_2d_angle(Vector_2D *v, Vector_2D *w){
	double result = vector_2d_dot(v,w);
	result /= (vector_2d_magnitude(v)*vector_2d_magnitude(w));
	return acos(result)*(180/M_PI);
} 

Vector_2D vector_2d_unary_i(){
	return unary_i;
}

Vector_2D vector_2d_unary_j(){
	return unary_j;
}



void vector_2d_dtor(Vector_2D *v){ 
	assert(v != NULL);
	if(v->head && v->head != &origin){ 
		free(v->head);	
	} 
	if(v->tail){
		free(v->tail);	
	} 
	free(v);
}
