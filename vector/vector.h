#ifndef __VECTOR_H
#define __VECTOR_H
#include "../point/point.h"

typedef struct {
	Point_2D *head;
	Point_2D *tail;
}Vector_2D; 

Vector_2D *vector_2d_ctor(Point_2D *head, Point_2D *tail);
Vector_2D *vector_2d_origin(Vector_2D *v);
Vector_2D *vector_2d_add(Vector_2D* v, Vector_2D* w);
Vector_2D *vector_2d_sub(Vector_2D* v, Vector_2D* w);
Vector_2D *vector_2d_mul(double scalar, Vector_2D* w);  
double vector_2d_magnitude(Vector_2D *v);
double vector_2d_angle(Vector_2D *v, Vector_2D *w); 
double vector_2d_dot(Vector_2D *v, Vector_2D *w);
Vector_2D vector_2d_unary_i();
Vector_2D vector_2d_unary_j(); 
void vector_2d_dtor(Vector_2D* v); 

#endif
