#include <X11/X.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <stdbool.h>
#include "util.h"
#include "vector/vector.h"

int main(int argc, char **argv){ 
	Display *dpy = XOpenDisplay(NULL);
	int scr = DefaultScreen(dpy);
	Window win = XCreateSimpleWindow(dpy, RootWindow(dpy, scr), 0,0,100,100,0,0x000000,0xFFFFFF);
	XMapWindow(dpy, win); 
	XSelectInput(dpy, win, ExposureMask);
	XEvent report;
	GC gc = DefaultGC(dpy, scr);
	XGCValues gcv;
	gcv.line_width = 2;
	XChangeGC(dpy, gc, GCLineWidth, &gcv);

	Vector_2D *v1 = vector_2d_ctor(point_2d_ctor(10,10), point_2d_ctor(50,50));
	Vector_2D *v2 = vector_2d_ctor(point_2d_ctor(-10,-10), point_2d_ctor(0,0));
	Vector_2D *v3 = vector_2d_add(v1,v2);

	int w, cw; 
	int h, ch;
	
	while(true){
		XNextEvent(dpy,&report);
		w = report.xexpose.width;
		h = report.xexpose.height;
		cw = w/2;
		ch = h/2;
		XDrawLine(dpy, win, gc, 0,h/2,w,h/2); 
		XDrawLine(dpy, win, gc, w/2,0,w/2,h);
		XSetForeground(dpy, gc, 0xFF0000);

		gcv.line_width = 1;
		XChangeGC(dpy, gc, GCLineWidth, &gcv);

		XDrawLine(dpy, win, gc, cw+v1->head->x,ch-v1->head->y,cw+v1->tail->x, ch-v1->tail->y);
		XDrawLine(dpy, win, gc, cw+v1->tail->x,ch-v1->tail->y,cw+v1->tail->x-10, ch-v1->tail->y-5);
		XDrawLine(dpy, win, gc, cw+v1->tail->x,ch-v1->tail->y,cw+v1->tail->x-10, ch-v1->tail->y+5);
/*
		XSetForeground(dpy, gc, 0x00F0FF);
		XDrawLine(dpy, win, gc, cw+v2->head->x,ch-v2->head->y,cw+v2->tail->x, ch-v2->tail->y);
		XSetForeground(dpy, gc, 0xF000FF);
		XDrawLine(dpy, win, gc, cw+v3->head->x,ch-v3->head->y,cw+v3->tail->x, ch-v3->tail->y);
		XSetForeground(dpy, gc, 0x000000);
		*/
		gcv.line_width = 1;
		XChangeGC(dpy, gc, GCLineWidth, &gcv);
	}

}  

