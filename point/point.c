#include <stdlib.h>
#include <assert.h>
#include "point.h"

Point_2D *point_2d_ctor(double x, double y){
	Point_2D *point = (Point_2D*)malloc(sizeof(Point_2D));
	point->x = x;
	point->y = y;
	return point;
} 

void point_2d_dtor(Point_2D *point){
	assert(point != NULL);
	free(point);
}
