#ifndef __POINT_H_
#define __POINT_H_

typedef struct{
	double x, y;
}Point_2D;

Point_2D *point_2d_ctor(double x, double y);
void point_2d_dtor(Point_2D *point);
#endif

